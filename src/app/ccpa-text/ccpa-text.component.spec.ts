import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcpaTextComponent } from './ccpa-text.component';

describe('CcpaTextComponent', () => {
  let component: CcpaTextComponent;
  let fixture: ComponentFixture<CcpaTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcpaTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcpaTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
