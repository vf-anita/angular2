import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CcpaTextComponent } from './ccpa-text/ccpa-text.component';
import { CcpaButtonComponent } from './ccpa-button/ccpa-button.component';

@NgModule({
  declarations: [
    AppComponent,
    CcpaTextComponent,
    CcpaButtonComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
