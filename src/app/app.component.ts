import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public CCPA_ACCEPTED: boolean = false;
 /** @type {boolean} */
  /** Accepts the CCPA closes the header */
  acceptCCPA = () => {
    this.CCPA_ACCEPTED = !this.CCPA_ACCEPTED;
  }
}
