import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcpaButtonComponent } from './ccpa-button.component';

describe('CcpaButtonComponent', () => {
  let component: CcpaButtonComponent;
  let fixture: ComponentFixture<CcpaButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcpaButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcpaButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
